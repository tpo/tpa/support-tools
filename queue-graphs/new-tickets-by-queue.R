# Import required packages
require(ggplot2)
require(RColorBrewer)

# Use English locale
Sys.setenv(LANGUAGE="en")
Sys.setlocale("LC_TIME", "en_US.UTF-8")

# Read the csv file passed as argument, but don't convert strings to factors
args <- commandArgs(trailingOnly = TRUE)
data <- read.csv(args[1], stringsAsFactors = FALSE)

# Format months as "month_name year", rather than "year-month"
data$month <- factor(data$month,
  labels = format(as.Date(paste(unique(data$month), "-01", sep = ""),
  format = "%Y-%m-%d"), "%B %Y"))

# Reorder queues so that default is first, and use better labels
data$queue <- factor(data$queue,
  levels = c("en", unique(data$queue[data$queue != "en"])),
  labels = list("en" = "Default queue",
                "ar" = "Arabic queue",
                "es" = "Spanish queue",
                "fa" = "Farsi queue",
                "fr" = "French queue",
                "zh" = "Chinese queue"))

# Plot the data
ggplot(data,
  aes(x = month,                  # Month on the x axis
      y = newtickets,             # Number of new tickets on y axis
      fill = queue)) +            # Different colours for queues
# Make a dodged bar chart
geom_bar(stat = "identity", position = "dodge") +
scale_x_discrete("") +            # No x axis label
scale_y_continuous("") +          # No y axis label
# No legend title
# For other color palettes, see http://colorbrewer2.org/ and
# http://vis.supstat.com/2013/04/plotting-symbols-and-color-palettes/
scale_fill_brewer("", palette = "Set2") +
ggtitle("Handled support tickets per month by queue\n") +
theme_bw()
ggsave(args[2], width = 8, height = 4, dpi = 100)

