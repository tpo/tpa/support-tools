Monthly reporting scripts
=========================

`monthly_stats.py` is meant to be run on rude.torproject.org and will
produce most statistics for a monthly help desk report.

Usage: monthly_stats.py [YEAR MONTH]

By default the report will be for the current month.

`~/.pgpass` should be properly filled, e.g.:

    drobovi.torproject.org:5432:rt:rtreader:A_SECRET_PASSWORD
