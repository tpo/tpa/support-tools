#!/usr/bin/python
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

import calendar
from datetime import datetime, timedelta
import psycopg2
import re
import subprocess
import sys

RT_CONNINFO = "host=drobovi.torproject.org sslmode=require user=rtreader dbname=rt"

SELECT_TRANSACTIONS_QUERY = """
    SELECT tickets.id, transactions.created, transactions.type, transactions.newvalue
      FROM tickets
      JOIN queues ON (queues.id = tickets.queue)
      JOIN transactions ON (transactions.objectid = tickets.id
                        AND transactions.objecttype = 'RT::Ticket')
     WHERE tickets.lastupdated >= %s
       AND tickets.created < %s + INTERVAL '1 MONTH'
     ORDER BY tickets.id, transactions.id;
"""

BUCKET_SIZE = 1 # hours

def account_tickets(con, year, month):
    cur = con.cursor()
    first_of_month = datetime(year, month, 1).date()
    print cur.mogrify(SELECT_TRANSACTIONS_QUERY, (first_of_month, first_of_month))
    cur.execute(SELECT_TRANSACTIONS_QUERY, (first_of_month, first_of_month))

    open_tickets = {} # ticket_id -> time
    response_times = {}
    fields = {}
    for ticket_id, transaction_created, transaction_type, transaction_new_value in cur:
        if transaction_type not in ('Create', 'Status'):
            continue
        status = transaction_new_value
        last_update = transaction_created
        print "%d %s %s" % (ticket_id, status, last_update)
        if status == 'resolved' or status == 'rejected':
            if ticket_id in open_tickets:
                response_time = last_update - open_tickets[ticket_id]
                bucket = int(response_time.total_seconds() / (BUCKET_SIZE * 60 * 60))
                response_times[bucket] = 1 + response_times.get(bucket, 0)
                print "%d answered in %s" % (ticket_id, response_time)
                del open_tickets[ticket_id]
            else:
                print "Don't know when %d was opened." % (ticket_id)
        else:
            if ticket_id not in open_tickets:
                open_tickets[ticket_id] = last_update
    cur.close()
    print ''
    buckets = response_times.keys()
    buckets.sort()
    print 'month,fromhours,tohours,requests'
    for bucket in buckets:
        print "%04d-%02d,%d,%d,%d" % (year, month, bucket * BUCKET_SIZE, (bucket + 1) * BUCKET_SIZE, response_times[bucket])

if __name__ == '__main__':
    if len(sys.argv) == 1:
        now = datetime.now()
        year, month = now.year, now.month
    elif len(sys.argv) == 3:
        year, month = int(sys.argv[1]), int(sys.argv[2])
    else:
        print >>sys.stderr, "Usage: %s [YEAR MONTH]" % (sys.argv[0])
        sys.exit(1)
    con = psycopg2.connect(RT_CONNINFO)
    account_tickets(con, year, month)
    con.close()
